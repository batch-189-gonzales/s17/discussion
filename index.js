// Declaring function
function sayHello() {
	console.log('Hello there!');
};

// Invoking function
sayHello();

// Setting function to a variable
let sayGoodbye = function() {
	console.log('Goodbye!');
};

sayGoodbye();

sayGoodbye = function() {
	console.log('Au Revior!');
};

sayGoodbye();

const sayHelloInJapanese = function() {
	console.log('Ohayo!');
};

sayHelloInJapanese();

// Global Scope - you can use a variable inside a function if the function is declared outside of the function
let action1 = 'Run';

function doSomethingRandom1() {
	console.log(action1);
};

doSomethingRandom1();

// Local Scope - you can only use a variable declared inside of a function within the function
function doSomethingRandom2() {
	let action2 = 'Rest';
	console.log(action2);
};

doSomethingRandom2();

//Nested Function - you can nest functions inside of a function as long as you invoke the child function within the parent function
function viewProduct() {
	console.log('Viewing Product');

	function addToCart() {
		console.log('Added product to cart');
	};

	addToCart();
};

viewProduct();

// Alert function - built-in js fxn where it displays alerts to the user
function singASong() {
	alert('La la la');
};

singASong();
console.log('Clap clap clap');

// Prompt function - built-in js fxn where it displays a dialog box that prompts the user for input
function enterUsertName() {
	let username = prompt('Enter you username');
	console.log(username);
};

enterUsertName();

let userAge = prompt('Enter your age');
console.log(userAge);

console.log(prompt('Enter your name'));